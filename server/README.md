# REST API

### Launch dev (Hot reload)
````bash
./gradlew bootRun
````
Default endpoint<br>
> http://localhost:8080

### Open API 3

Dependencie
> https://springdoc.org

**JSON** format
```
/v3/api-docs
```

**HTML** format
```
/swagger-ui.html
```