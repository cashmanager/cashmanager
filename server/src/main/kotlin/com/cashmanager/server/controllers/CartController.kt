package com.cashmanager.server.controllers

import com.cashmanager.server.models.CartDAO
import com.cashmanager.server.models.CartDTO
import com.cashmanager.server.models.CartRepository
import com.cashmanager.server.models.CreateCartDTO
import com.cashmanager.server.services.CartService
import org.springframework.data.repository.query.Param
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("cart")
public class CartController(private val cartRepository: CartRepository, private val cartService: CartService) {

    @PostMapping("/")
    fun createCart(@RequestHeader("Authorization") id_user: Long): CartDTO {
        val input: CreateCartDTO = CreateCartDTO(id_user, "OPEN")

        return this.cartRepository.save(CartDAO(input)).toCartDTO()
    }

    @GetMapping("/all")
    fun getAllCarts(@RequestHeader("Authorization") id_user: Long): ArrayList<CartDTO> {
        return this.cartService.getAllUserCarts(id_user)
    }

    @GetMapping("/open")
    fun getOpenedCart(@RequestHeader("Authorization") id_user: Long): CartDTO {
        return this.cartService.getOpenedCart(id_user)
    }

    @PutMapping("/toggle/{id}")
    fun toggleCart(@Param("id") id: Long): CartDTO {
        return this.cartService.toggleCart(id)
    }

    @DeleteMapping("/{id}")
    fun deleteCart(@Param("id") id: Long) {
        return this.cartRepository.deleteById(id)
    }

    /* @GetMapping("/{id}")
    fun getById(@Param("id") id: Long): CartDTO {
        return this.cartRepository.findById(id).get().toCartDTO()
    }

    @PostMapping("/")
    fun createCart(@RequestBody input: CreateCartDTO): CartDTO {
        return this.cartRepository.save(CartDAO(input)).toCartDTO()
    }

    @DeleteMapping("/{id}")
    fun deleteCart(@Param("id") id: Long) {
        return this.cartRepository.deleteById(id)
    }

    @PutMapping("/{id}")
    fun updateCart(@Param("id") id: Long, @RequestBody input: CreateCartDTO): CartDTO {
        return this.cartRepository.save(CartDAO(input)).toCartDTO()
    } */
}
