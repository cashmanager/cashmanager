package com.cashmanager.server.controllers

import com.cashmanager.server.models.ProductDTO
import com.cashmanager.server.models.ProductRepository
import org.springframework.data.repository.query.Param
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("products")
public class ProductController(private val productRepository: ProductRepository) {

    @GetMapping("/{id}")
    fun getById(@Param("id") id: Long): ProductDTO {
        return this.productRepository.findById(id).get().toProductDTO()
    }
}
