package com.cashmanager.server.controllers

import com.cashmanager.server.models.ArticleDAO
import com.cashmanager.server.models.ArticleDTO
import com.cashmanager.server.models.ArticleRepository
import com.cashmanager.server.models.CreateArticleDTO
import org.springframework.data.repository.query.Param
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("articles")
public class ArticleController(private val articleRepository: ArticleRepository) {

    @GetMapping("/{cart}/all")
    fun getById(@Param("cart") id_cart: Long): Iterable<ArticleDTO> {
        return this.articleRepository.findByCart(id_cart)
    }

    @PostMapping("/")
    fun createArticle(@RequestBody input: CreateArticleDTO): ArticleDTO {
        return this.articleRepository.save(ArticleDAO(input)).toArticleDTO()
    }

    @PutMapping("/{id}")
    fun updateCart(@Param("id") id: Long, @RequestBody input: CreateArticleDTO): ArticleDTO {
        return this.articleRepository.save(ArticleDAO(input)).toArticleDTO()
    }

    @DeleteMapping("/{id}")
    fun deleteArticle(@Param("id") id: Long) {
        return this.articleRepository.deleteById(id)
    }
}
