package com.cashmanager.server.services

import com.cashmanager.server.models.CartDAO
import com.cashmanager.server.models.CartDTO
import com.cashmanager.server.models.CartRepository
import com.cashmanager.server.models.CreateCartDTO
import kotlin.collections.ArrayList
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service

@Service
public class CartService(private val cartRepository: CartRepository) {
    fun getOpenedCart(@Param("id_user") id_user: Long): CartDTO {
        val carts = cartRepository.findAll()

        for (cart in carts) {
            if (cart.id_user == id_user && cart.status == "OPEN")
                return cart.toCartDTO()
        }
        return CartDTO(-1, -1, "No cart opened yet")
    }

    fun getAllUserCarts(@Param("id_user") id_user: Long): ArrayList<CartDTO> {
        val carts = cartRepository.findAll()
        val user_carts: ArrayList<CartDTO> = ArrayList<CartDTO>()

        for (cart in carts) {
            if (cart.id_user == id_user)
                user_carts.add(cart.toCartDTO())
        }

        return user_carts
    }

    fun toggleCart(@Param("id") id: Long): CartDTO {
        val cart: CartDTO = this.cartRepository.findById(id).get().toCartDTO()

        if (cart.status == "OPEN")
            cart.status = "CLOSE"
        else
            cart.status = "OPEN"

        val update_cart: CreateCartDTO = CreateCartDTO(cart.id_user, cart.status)

        return this.cartRepository.save(CartDAO(update_cart)).toCartDTO()
    }
}
