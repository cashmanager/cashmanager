package com.cashmanager.server.models

import javax.persistence.*
import javax.validation.constraints.Size
import org.springframework.data.repository.CrudRepository

data class CreateUserDTO(
    var email: String,
    var firstname: String,
    var lastname: String,
    var password: String
)

data class UserDTO(
    var id: Long,
    var email: String,
    var firstname: String,
    var lastname: String
)

data class UserLoginDTO(
    var email: String,
    var password: String
)

@Entity
@Table(name = "Users")
data class UserDAO(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = -1,

    @Column(name = "email", nullable = false, unique = true)
    @Size(min = 5, max = 254)
    var email: String = "",

    @Column(name = "firstname", nullable = false)
    @Size(min = 3, max = 64)
    var firstname: String = "",

    @Column(name = "lastname", nullable = false)
    @Size(min = 3, max = 64)
    var lastname: String = "",

    @Column(name = "password_hash", nullable = false)
    var passwordHash: String = ""

) {
    constructor(input: CreateUserDTO) : this(
            email = input.email,
            firstname = input.firstname,
            lastname = input.lastname,
            passwordHash = input.password
    ) {
        // this.hashPassword()
    }

    /* fun hashPassword() {
        var bCryptPasswordEncoder = BCryptPasswordEncoder()
        this.passwordHash = bCryptPasswordEncoder.encode(this.passwordHash)
    } */

    fun toUserDTO(): UserDTO {
        return UserDTO(this.id, this.email, this.firstname, this.lastname)
    }
}

interface UserRepository : CrudRepository<UserDAO, Long> {
    fun findByEmail(email: String): UserDAO
}
