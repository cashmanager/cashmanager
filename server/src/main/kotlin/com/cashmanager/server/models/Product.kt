package com.cashmanager.server.models

import javax.persistence.*
import javax.validation.constraints.Size
import org.springframework.data.repository.CrudRepository

data class ProductDTO(
    var id: Long,
    var name: String,
    var price: Number,
    var description: String
)

@Entity
@Table(name = "Products")
data class ProductDAO(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = -1,

    @Column(name = "name", nullable = false)
    @Size(min = 3, max = 64)
    var name: String = "",

    @Column(name = "price", nullable = false)
    var price: Number = 0.00F,

    @Column(name = "description", nullable = false)
    var description: String = ""
) {
    fun toProductDTO(): ProductDTO {
        return ProductDTO(this.id, this.name, this.price, this.description)
    }
}

interface ProductRepository : CrudRepository<ProductDAO, Long>
