package com.cashmanager.server.models

import javax.persistence.*
import org.springframework.data.repository.CrudRepository

data class CreateCartDTO(
    var id_user: Long,
    var status: String
)

data class CartDTO(
    var id: Long,
    var id_user: Long,
    var status: String
)

@Entity
@Table(name = "Cart")
data class CartDAO(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = -1,

    @Column(name = "id_user", nullable = false)
    var id_user: Long = -1,

    @Column(name = "status", nullable = false)
    var status: String = "OPEN"
) {
    constructor(input: CreateCartDTO) : this(
        id_user = input.id_user,
        status = input.status
    )

    fun toCartDTO(): CartDTO {
        return CartDTO(this.id, this.id_user, this.status)
    }
}

interface CartRepository : CrudRepository<CartDAO, Long>
