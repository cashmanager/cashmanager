package com.cashmanager.server.models

import javax.persistence.*
import org.springframework.data.repository.CrudRepository

data class CreateArticleDTO(
    var id: Long,
    var cart: Long,
    var product: Long,
    var quantity: Number
)

data class ArticleDTO(
    var id: Long,
    var cart: Long,
    var product: Long,
    var quantity: Number
)

@Entity
@Table(name = "Article")
data class ArticleDAO(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = -1,

    @Column(name = "cart", nullable = false)
    var cart: Long = -1,

    @Column(name = "product", nullable = false)
    var product: Long = -1,

    @Column(name = "quantity", nullable = false)
    var quantity: Number = 0
) {
    constructor(input: CreateArticleDTO) : this(
        cart = input.cart,
        product = input.product,
        quantity = input.quantity
    )

    fun toArticleDTO(): ArticleDTO {
        return ArticleDTO(this.id, this.cart, this.product, this.quantity)
    }
}

interface ArticleRepository : CrudRepository<ArticleDAO, Long> {
    fun findByCart(cart: Long): Iterable<ArticleDTO>
}
