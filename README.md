# Cashmanager

```bash
docker-compose up
```
#### Accès à l'application mobile
[http://localhost:8081/client.apk](http://localhost:8081/client.apk)
# Back-end

## Endpoints

With our API we can manage Products, Carts, Articles (products placed in cart) and create a User with the following endpoints.

![Bakend endpoints](endpoints.png)

#### Accès à la documentation Swagger de l'API
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)