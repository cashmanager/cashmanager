import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { ApiModule } from 'api-client';
import { AppComponent } from './components/app/app.component';
import { routes } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SigninPageComponent } from './pages/authentication/signin-page/signin-page.component';
import { SigninComponent } from './components/authentication/signin/signin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupPageComponent } from './pages/authentication/signup-page/signup-page.component';
import { SignupComponent } from './components/authentication/signup/signup.component';
import { BottomToolbarComponent } from './components/app/bottom-toolbar/bottom-toolbar.component';
import { UpperToolbarComponent } from './components/app/upper-toolbar/upper-toolbar.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { BottomMenuComponent } from './components/app/bottom-menu/bottom-menu.component';
import { NfcPageComponent } from './pages/nfc-page/nfc-page.component';
import { Ndef, NFC } from '@ionic-native/nfc/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

@NgModule({
  declarations: [
    AppComponent,
    SigninPageComponent,
    SigninComponent,
    SignupPageComponent,
    SignupComponent,
    BottomToolbarComponent,
    UpperToolbarComponent,
    HomePageComponent,
    NfcPageComponent,
    BottomMenuComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    IonicModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ApiModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatInputModule,
    MatMenuModule,
    MatProgressSpinnerModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    NFC,
    Ndef,
    AndroidPermissions,
    OpenNativeSettings
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
