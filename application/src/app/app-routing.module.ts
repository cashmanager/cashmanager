
import { SignupPageComponent } from './pages/authentication/signup-page/signup-page.component';
import { SigninPageComponent } from './pages/authentication/signin-page/signin-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { Routes } from '@angular/router';
import { NfcPageComponent } from './pages/nfc-page/nfc-page.component';

export const routes: Routes = [
  { path: '', redirectTo: 'sign_in', pathMatch: 'full' },
  { path: 'sign_in', component: SigninPageComponent, data: { animation: 'SigninPage' }},
  { path: 'sign_up', component: SignupPageComponent, data: { animation: 'SignupPage' }},
  { path: 'home', component: HomePageComponent, data: { animation: 'HomePage' }},
  { path: 'nfc', component: NfcPageComponent, data: { animation: 'NfcPage' }}
];
