import { Platform } from '@angular/cdk/platform';
import { Injectable } from '@angular/core';
import { NFC, NfcTag } from '@ionic-native/nfc/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

@Injectable({
  providedIn: 'root'
})
export class NfcService {

  private androidFlags: number;

  constructor(private platform: Platform,
              private nfc: NFC,
              private phoneSettings: OpenNativeSettings
  ) {
    this.androidFlags = this.nfc.FLAG_READER_NFC_A |
                        this.nfc.FLAG_READER_NFC_V |
                        this.nfc.FLAG_READER_NFC_F |
                        this.nfc.FLAG_READER_NFC_V |
                        this.nfc.FLAG_READER_NFC_BARCODE;
  }

  private readIOS() {
    return this.nfc.scanNdef();
  }

  private async readAndroid(): Promise<NfcTag> {
    return new Promise((resolve, reject) => {
        let readerAndroid = this.nfc.readerMode(this.androidFlags);
        readerAndroid.subscribe(res => resolve(res), err => reject(err));
    });
  }

  async readNfc(): Promise<NfcTag> {
    if (this.platform.ANDROID) {
      return await this.readAndroid();
    } else if (this.platform.IOS) {
      return await this.readIOS();
    }
    throw new Error('NCF not supported');
  }

  hasNFCEnabled(): Promise<boolean> {
    return new Promise(resolve => {
      this.nfc.enabled()
      .then(value => {
        resolve(value === 'NFC_OK');
      })
      .catch(() => {
        resolve(false);
      });
    });
  }

  openAndroidNFCSettings(): Promise<any> {
    return this.phoneSettings.open('nfc_settings');
  }

  hasNFC(): Promise<boolean> {
    return new Promise(resolve => {
      this.nfc.enabled()
      .then(value => {
        resolve(value === 'NFC_OK');
      })
      .catch(err => {
        resolve(err === 'NFC_DISABLED');
      });
    });
  }
}
