import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ResolveEnd, Router, RouterEvent, RouterOutlet } from '@angular/router';
import { slideInAnimation } from './animation';
import { NfcService } from 'src/app/services/nfc/nfc.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  animations: [
    slideInAnimation
  ]
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _router: Router,
    private _nfcService: NfcService
  ) {
    this.initializeApp();
  }

  public pages = [
    {
      pages: [
        { name: 'Home', icon: 'home', func: () => this._router.navigateByUrl('/home') },
      ]
    }
  ];
  public showUi = false;

  private initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this._router.events.subscribe((event: RouterEvent) => {
        if (event instanceof ResolveEnd) {
          const url = event.urlAfterRedirects;
          this.showUi = !url.startsWith('/sign_');
        }
      });
    });
  }

  public logout() {
    this._router.navigateByUrl('/sign_in');
  }

  public prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
