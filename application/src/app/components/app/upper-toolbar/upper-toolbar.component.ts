import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-upper-toolbar',
  templateUrl: './upper-toolbar.component.html',
  styleUrls: ['./upper-toolbar.component.scss'],
})
export class UpperToolbarComponent {

  @Output() public menuToogle = new EventEmitter<any>();

}
