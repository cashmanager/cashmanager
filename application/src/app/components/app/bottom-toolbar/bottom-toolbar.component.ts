import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NfcService } from 'src/app/services/nfc/nfc.service';

@Component({
  selector: 'app-bottom-toolbar',
  templateUrl: './bottom-toolbar.component.html',
  styleUrls: ['./bottom-toolbar.component.scss'],
})
export class BottomToolbarComponent {

  public hasNFC = false;

  constructor(
    private _router: Router,
    private _nfc: NfcService
    )
  {
    this._nfc.hasNFC()
      .then(value => {
        this.hasNFC = value;
      });
  }

  @Output() onToggle = new EventEmitter<Boolean>();

  public hide = true;

  public onToogle() {
    this.hide = !this.hide;
    this.onToggle.emit(this.hide);
  }

  public goTo(path: string) {
    this._router.navigateByUrl(path);
  }
}
