import { animateChild, query, style, transition, trigger, group, animate } from '@angular/animations';

const toLeftAnimation = [
  style({ position: 'relative' }),
  query(':enter, :leave', [
    style({
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%'
    })
  ], { optional: true }),
  query(':enter', [
    style({ left: '100%' })
  ]),
  query(':leave', animateChild(), { optional: true }),
  group([
    query(':leave', [
      animate('300ms ease-out', style({ left: '-100%' }))
    ], { optional: true }),
    query(':enter', [
      animate('300ms ease-out', style({ left: '0%' }))
    ])
  ]),
  query(':enter', animateChild()),
]

const toRightAnimation = [
  style({ position: 'relative' }),
  query(':enter, :leave', [
    style({
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%'
    })
  ], { optional: true }),
  query(':enter', [
    style({ left: '-100%' })
  ]),
  query(':leave', animateChild(), { optional: true }),
  group([
    query(':leave', [
      animate('300ms ease-out', style({ left: '100%' }))
    ], { optional: true }),
    query(':enter', [
      animate('300ms ease-out', style({ left: '0%' }))
    ])
  ]),
  query(':enter', animateChild()),
]

export const slideInAnimation =
  trigger('routeAnimations', [
    transition('SigninPage => SignupPage', toLeftAnimation),
    transition('* => SigninPage', toRightAnimation),
    transition('* => NfcPage', toLeftAnimation),
    // transition('NfcPage => ScanSuccess', toLeftAnimation),
    transition('NfcPage => *', toRightAnimation)
  ]);
