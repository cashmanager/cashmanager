import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { passwordConfirmation } from 'src/app/utils/validators/password-confirmation.validator';

export interface SignUpForm {
  firstname: string,
  lastname: string,
  email: string,
  password: string
};

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {

  @Output() onSubmit = new EventEmitter<SignUpForm>();

  public signupFormGroup: FormGroup;
  public hide = true;

  constructor(private fb: FormBuilder) {
    this.signupFormGroup = this.fb.group({
      firstname: new FormControl(
        '',
        Validators.required
      ),
      lastname: new FormControl(
        '',
        Validators.required
      ),
      email: new FormControl(
        '',
        [
          Validators.required,
          Validators.email
        ]
      ),
      password: new FormControl(
        '',
        Validators.required,
      ),
      passwordConfirm: new FormControl(
        '',
        Validators.required
      )
    }, {
      validators: passwordConfirmation('password', 'passwordConfirm')
    });
  }

  public submit() {
    this.onSubmit.emit(this.signupFormGroup.value);
  }
}
