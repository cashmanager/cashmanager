import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

export interface LoginForm {
  email: string,
  password: string
};

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent {

  @Output() onSubmit = new EventEmitter<LoginForm>();

  public loginFormGroup: FormGroup;
  public hide = true;

  constructor(private fb: FormBuilder) {
    this.loginFormGroup = this.fb.group({
      email: new FormControl(
        '',
        [
          Validators.required,
          Validators.email
        ]),
      password: new FormControl(
        '',
        Validators.required
        )
    })
  }

  public submit() {
    this.onSubmit.emit(this.loginFormGroup.value);
  }
}
