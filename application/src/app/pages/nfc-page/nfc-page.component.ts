import { Platform } from '@angular/cdk/platform';
import { Component, OnInit } from '@angular/core';
import { NfcService } from 'src/app/services/nfc/nfc.service';

enum NfcStatus {
  ToBeEnabled = 'ToBeEnabled',
  Enabled = 'Enabled'
}

@Component({
  selector: 'app-nfc-page',
  templateUrl: './nfc-page.component.html',
  styleUrls: ['./nfc-page.component.scss'],
})
export class NfcPageComponent implements OnInit {

  public nfcStatus = NfcStatus.ToBeEnabled;

  constructor(
    private _nfc: NfcService,
    public _platform: Platform
  ) { }

  ngOnInit() {
    this.refreshNFCStatus();
  }

  public openSettings() {
    document.addEventListener('resume', () => {
      document.removeEventListener('resume', () => {});
      this.refreshNFCStatus();
    });
    this._nfc.openAndroidNFCSettings();
  }

  private refreshNFCStatus() {
    this._nfc.hasNFCEnabled()
      .then(value => {
        this.nfcStatus = value ? NfcStatus.Enabled : NfcStatus.ToBeEnabled;
        if (this.nfcStatus === NfcStatus.Enabled) {
          this._nfc.readNfc()
            .then(tag => {
              console.log(tag);
            });
        }
      });
  }
}


    // this.nfcService.readNfc()
    //   .then(tag => {
    //     console.log(tag);
    //   })
    //   .catch(err => console.log(err));
