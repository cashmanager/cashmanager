import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NfcPageComponent } from './nfc-page.component';

describe('NfcPageComponent', () => {
  let component: NfcPageComponent;
  let fixture: ComponentFixture<NfcPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NfcPageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NfcPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
