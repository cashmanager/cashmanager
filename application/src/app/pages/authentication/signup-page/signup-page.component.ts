import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SignUpForm } from 'src/app/components/authentication/signup/signup.component';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss'],
})
export class SignupPageComponent {

  constructor(private _router: Router) {

  }

  onSignup(value: SignUpForm) {
    console.log("TODO");
    console.log(value);
    this._router.navigateByUrl('/home');
  }
}
