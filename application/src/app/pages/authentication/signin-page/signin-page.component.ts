import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LoginForm } from '../../../components/authentication/signin/signin.component';

@Component({
  selector: 'app-signin-page',
  templateUrl: './signin-page.component.html',
  styleUrls: ['./signin-page.component.scss'],
})
export class SigninPageComponent{

  constructor(private _router: Router) {

  }

  public onLogin(value: LoginForm) {
    console.log("TODO");
    console.log(value);
    this._router.navigateByUrl('/home');
  }
}
