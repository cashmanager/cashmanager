# REST API

### Installation
```bash
npm i
```

### Launch dev (Hot reload)
> https://ionicframework.com/docs/cli/livereload

### Update swagger api/services
```bash
npm run gen-swagger
```

### First launch
```bash
npm run build
```

> For Android, you need android sdk and theses env var
```bash
ANDROID_SDK_ROOT # Path to android sdk
# PATH binaries
$ANDROID_SDK_ROOT/platform-tools
$ANDROID_SDK_ROOT/tools/bin
$ANDROID_SDK_ROOT/emulator
$ANDROID_SDK_ROOT/build-tools
```

```bash
# Android
ionic capacitor run android -l --external

# Ios
pod install --project-directory=ios/App
ionic capacitor run ios -l --external
```
